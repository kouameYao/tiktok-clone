import { SuggestedAccount } from "../types";

export const SuggestedAccountsData: SuggestedAccount[] = [
  {
    id: "1",
    profileUrl: "https://p16-sign-va.tiktokcdn.com/tos-maliva-avt-0068/69638c4194ebafc1665fcc014c7dee9b~c5_100x100.jpeg?x-expires=1678654800&x-signature=SqA1s7cp2TvEWuSJTrutmPwwx4o%3D",
    username: "Diomande souleymane",
    nickname: "D_Souley",
    isPremium: true,
    likes: 12.34,
    follower: 23.0,
  },
  {
    id: "2",
    profileUrl: "https://p16-sign-va.tiktokcdn.com/tos-maliva-avt-0068/5ca0125b4f3660dde4e6ab5833ecf196~c5_100x100.jpeg?x-expires=1678658400&x-signature=ZGDQk7gGI6%2BcP8FmYMtB2c%2FMuXI%3D",
    username: "Soro Nahagama",
    nickname: "S_Naha",
    isPremium: true,
    likes: 234,
    follower: 290,
  },
  {
    id: "3",
    profileUrl: "https://p16-sign-va.tiktokcdn.com/musically-maliva-obj/89ff10f7452bfac0269cee5f64270830~c5_100x100.jpeg?x-expires=1678654800&x-signature=RBjCE9BMnUeaGJnPc6LYCzZRBlI%3D",
    username: "Kouame Yao",
    nickname: "K_yao",
    isPremium: true,
    likes: 12.34,
    follower: 23.0
  },
  {
    id: "4",
    profileUrl: "https://p16-sign-sg.tiktokcdn.com/aweme/100x100/tiktok-obj/da7597d2feadb097f7604f57e59605e8.jpeg?x-expires=1678654800&x-signature=NSnTLQwkvXf9G2jEIaBrFu3PEYQ%3D",
    username: "Konan Yannick",
    nickname: "Nanok P",
    isPremium: false,
    likes: 12.34,
    follower: 23.0
  },
  {
    id: "5",
    profileUrl: "https://p16-sign-va.tiktokcdn.com/tos-maliva-avt-0068/69638c4194ebafc1665fcc014c7dee9b~c5_100x100.jpeg?x-expires=1678654800&x-signature=SqA1s7cp2TvEWuSJTrutmPwwx4o%3D",
    username: "Dioname souleymane",
    nickname: "D_Souley",
    isPremium: false,
    likes: 12340,
    follower: 23.0
  },
  {
    id: "6",
    profileUrl: "https://p16-sign-va.tiktokcdn.com/tos-maliva-avt-0068/5ca0125b4f3660dde4e6ab5833ecf196~c5_100x100.jpeg?x-expires=1678658400&x-signature=ZGDQk7gGI6%2BcP8FmYMtB2c%2FMuXI%3D",
    username: "Soro Nahagame",
    nickname: "S_Naha",
    isPremium: false,
    likes: 0,
    follower: 2
  },
  {
    id: "7",
    profileUrl: "https://p16-sign-va.tiktokcdn.com/musically-maliva-obj/89ff10f7452bfac0269cee5f64270830~c5_100x100.jpeg?x-expires=1678654800&x-signature=RBjCE9BMnUeaGJnPc6LYCzZRBlI%3D",
    username: "Kouame Yao",
    nickname: "K_yao",
    isPremium: false,
    likes: 14,
    follower: 0
  },
  {
    id: "8",
    profileUrl: "https://p16-sign-sg.tiktokcdn.com/aweme/100x100/tiktok-obj/da7597d2feadb097f7604f57e59605e8.jpeg?x-expires=1678654800&x-signature=NSnTLQwkvXf9G2jEIaBrFu3PEYQ%3D",
    username: "Konan Yannick",
    nickname: "Nanok P",
    isPremium: true,
    likes: 1.4,
    follower: 2.0
  },
  {
    id: "9",
    profileUrl: "https://p16-sign-va.tiktokcdn.com/tos-maliva-avt-0068/69638c4194ebafc1665fcc014c7dee9b~c5_100x100.jpeg?x-expires=1678654800&x-signature=SqA1s7cp2TvEWuSJTrutmPwwx4o%3D",
    username: "Dioname souleymane",
    nickname: "D_Souley",
    isPremium: true,
    likes: 1.34,
    follower: 2.0
  },
  {
    id: "10",
    profileUrl: "https://p16-sign-va.tiktokcdn.com/tos-maliva-avt-0068/5ca0125b4f3660dde4e6ab5833ecf196~c5_100x100.jpeg?x-expires=1678658400&x-signature=ZGDQk7gGI6%2BcP8FmYMtB2c%2FMuXI%3D",
    username: "Soro Nahagame",
    nickname: "S_Naha",
    isPremium: true,
    likes: 123.4,
    follower: 20
  },
  {
    id: "11",
    profileUrl: "https://p16-sign-va.tiktokcdn.com/musically-maliva-obj/89ff10f7452bfac0269cee5f64270830~c5_100x100.jpeg?x-expires=1678654800&x-signature=RBjCE9BMnUeaGJnPc6LYCzZRBlI%3D",
    username: "Kouame Yao",
    nickname: "K_yao",
    isPremium: false,
    likes: 34,
    follower: 2
  },
  {
    id: "12",
    profileUrl: "https://p16-sign-sg.tiktokcdn.com/aweme/100x100/tiktok-obj/da7597d2feadb097f7604f57e59605e8.jpeg?x-expires=1678654800&x-signature=NSnTLQwkvXf9G2jEIaBrFu3PEYQ%3D",
    username: "Konan Yannick",
    nickname: "Nanok P",
    isPremium: false,
    likes: 1.34,
    follower: 3.0
  },
];