import { Avatar, Stack, Typography } from "@mui/material";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";

import { SuggestedAccount } from "../types";
import { LightTooltip } from "../styles/LightTooltip";
import MiniProfileCard from "./MiniProfileCard";

export default function SuggestedAccountsItem({
  account,
}: {
  account: SuggestedAccount;
}) {
  return (
    <LightTooltip
      key={account?.id}
      title={<MiniProfileCard account={account} />}
      placement="bottom"
    >
      <Stack
        direction="row"
        spacing={1}
        marginBottom={2}
        display="flex"
        alignItems="center"
      >
        <Avatar
          alt="Photo de profil"
          sx={{ width: 35, height: 35 }}
          src={account?.profileUrl}
        />
        <Stack direction="column">
          <Stack
            direction="row"
            spacing={0.5}
            display="flex"
            alignItems="center"
          >
            <Typography variant="h4" sx={{ fontWeight: 600, fontSize: 15 }}>
              {account?.username}
            </Typography>
            {account?.isPremium && (
              <CheckCircleIcon
                sx={{ color: "rgb(32, 213, 236)", width: 15, height: 15 }}
              />
            )}
          </Stack>
          <Typography variant="caption">{account?.nickname}</Typography>
        </Stack>
      </Stack>
    </LightTooltip>
  );
}
