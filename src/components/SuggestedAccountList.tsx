import React from "react";
import { Typography } from "@mui/material";

import SuggestedAccountItem from "./SuggestedAccountItem";
import { SuggestedAccountsData } from "../dummy/suggested-accounts";

export default function SuggestedAccountList() {
  const [seeMore, setSeeMore] = React.useState(false);

  return (
    <div>
      {!seeMore &&
        SuggestedAccountsData?.slice(0, 4).map((account) => (
          <SuggestedAccountItem key={account?.id} account={account} />
        ))}

      {seeMore &&
        SuggestedAccountsData?.map((account) => (
          <SuggestedAccountItem account={account} key={account?.id} />
        ))}

      <Typography
        sx={{
          cursor: "pointer",
          color: "rgb(254, 44, 85)",
          fontSize: 12,
          marginTop: 1,
        }}
        onClick={() => setSeeMore(!seeMore)}
      >
        {!seeMore ? "Voir plus" : "Voir moins"}
      </Typography>
    </div>
  );
}
