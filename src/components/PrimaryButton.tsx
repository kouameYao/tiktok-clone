import Button, { ButtonProps } from "@mui/material/Button";

import { PrimaryButtonStyle } from "../styles";

export type PrimaryButtonProps = ButtonProps & {
  title: string;
};

export default function PrimaryButton({ title, ...rest }: PrimaryButtonProps) {
  return (
    <Button
      {...rest}
      disableElevation
      sx={PrimaryButtonStyle}
      variant="contained"
    >
      {title}
    </Button>
  );
}
