import { Box, Stack, Typography } from "@mui/material";

import SuggestedAccountList from "./SuggestedAccountList";

export default function SuggestedAccounts() {
  return (
    <Box>
      <Typography variant="body1" sx={{ color: "rgba(22, 24, 35, .5)" }}>
        Comptes suggérés
      </Typography>
      <Stack direction="column" marginTop={2} marginBottom={2}>
        <SuggestedAccountList />
      </Stack>
    </Box>
  );
}
