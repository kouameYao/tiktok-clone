import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import Avatar from "@mui/material/Avatar";
import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import { SuggestedAccount } from "../types";

import PrimaryButton from "./PrimaryButton";

export default function MiniProfileCard({
  account,
}: {
  account: SuggestedAccount;
}) {
  return (
    <Box sx={{ minWidth: 275, paddingX: 1, paddingY: 2 }}>
      <Stack
        direction="row"
        spacing={1}
        marginBottom={2}
        display="flex"
        alignItems="center"
        justifyContent="space-between"
      >
        <Avatar
          alt="Photo de profil"
          sx={{ width: 45, height: 45 }}
          src={account?.profileUrl}
        />
        <PrimaryButton title="Suivre" size="small" />
      </Stack>
      <Stack direction="column">
        <Stack direction="row" spacing={0.5} display="flex" alignItems="center">
          <Typography variant="h4" sx={{ fontWeight: 600, fontSize: 15 }}>
            {account?.username}
          </Typography>
          {account?.isPremium && (
            <CheckCircleIcon
              sx={{ color: "rgb(32, 213, 236)", width: 15, height: 15 }}
            />
          )}
        </Stack>
        <Typography variant="caption">{account?.nickname}</Typography>
      </Stack>
      <Stack direction="row" spacing={0.5} display="flex" alignItems="center">
        <Stack direction="row" spacing={0.5} display="flex" alignItems="center">
          <Typography variant="h4" sx={{ fontSize: "14px", fontWeight: 600 }}>
            {account?.follower}M
          </Typography>
          <Typography variant="subtitle1">Abonnés</Typography>
        </Stack>
        <Stack direction="row" spacing={0.5} display="flex" alignItems="center">
          <Typography variant="h4" sx={{ fontSize: "14px", fontWeight: 600 }}>
            {account?.likes}M
          </Typography>
          <Typography variant="subtitle1">Abonnés</Typography>
        </Stack>
      </Stack>
    </Box>
  );
}
