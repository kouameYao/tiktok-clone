export type SuggestedAccount = {
  id: string;
  profileUrl: string;
  username: string;
  nickname: string;
  isPremium: boolean;
  follower: number;
  likes: number;
}
