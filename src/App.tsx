import React from 'react';
import './App.css';
import Header from './components/Header';
import { Stack } from '@mui/material';
import Sidebar from './components/Sidebar';
import MainContent from './components/MainContent';
function App() {
  return (
    <div>
      <Header />

      <Stack
        px={18}
        pt="90px"
        position="relative"
        display="flex"
        direction="row"
        spacing={3.5}
      >
        <Sidebar />

        <MainContent />
      </Stack>
    </div>
  );
}

export default App;
